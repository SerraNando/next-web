import Header from '@components/Header';
import Nav from '@common/Nav';
import Meta from '@common/Meta';

export default function MainLayout({ meta, children }) {
  return (
    <>
      <div className="min-h-full" style={{ paddingTop: '25px' }}>
        <Meta props={meta} />
        <Nav />
        <Header />

        <main>
          <div className="max-w-7xl mx-auto py-6 sm:px-6 lg:px-8">{children}</div>
        </main>
      </div>
      {/* <Footer/> */}
    </>
  );
}
