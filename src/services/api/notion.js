import { Client } from '@notionhq/client';

const getDataBeses = async (databaseId) => {
  const notion = new Client({ auth: process.env.NEXT_PUBLIC_API_TOKEN });
  try {
    const response = await notion.databases.query({
      database_id: databaseId,
      filter: {
        property: 'Published',
        checkbox: {
          equals: true,
        },
      },
    });
    return response.results;
  } catch (error) {
    console.log('error home', error);
  }
};

export { getDataBeses };
