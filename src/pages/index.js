import Image from 'next/image';
import { defaultMetaProps } from '@common/Meta';
import satellite from '@images/satellite.svg';
import Footer from '@common/Footer';
import { getDataBeses } from '@services/api/notion';
import Clients from '@components/Client';

/**
 * It fetches data from the API and returns it as props
 * @returns The function getStaticProps() is returning an object with the properties props.
 */
export async function getStaticProps() {
  const databaseIdStructures = '92228641257b46519a46aa8ac61fc066';
  const structures = await getDataBeses(databaseIdStructures);
  const databaseIdClients = 'f4af9986c7a34991ae601173319c18a8';
  const clients = await getDataBeses(databaseIdClients);
  let title1 = '';
  let title2 = '';
  let clientsFilter = [];

  for (let index = 0; index < structures.length; index++) {
    const element = structures[index];
    if (element.properties.Type.select.name === 'H1') {
      title1 = element.properties.Description.rich_text[0].plain_text;
    }
    if (element.properties.Type.select.name === 'H2') {
      title2 = element.properties.Description.rich_text[0].plain_text;
    }
  }
  for (let index = 0; index < clients.length; index++) {
    const element = clients[index];
    if (element.properties.Published.checkbox === true) {
      // console.log('entre',element)
      element.properties.id = element.id;

      clientsFilter.push(element.properties);
    }
  }
  // console.log('entre',clientsFilter[0].Name.title[0].plain_text)

  return {
    props: {
      title1: title1,
      title2: title2,
      clients,
      clientsFilter,
      meta: defaultMetaProps,
    },
  };
}

export default function Home({ title1, title2, clients }) {
  return (
    <div className="relative overflow-hidden mt-10 ml-10 lr-10">
      <div className="max-w-7xl mx-auto">
        <p className="text-white text-3xl max-w-4xl ">{title1}</p>
        <p className="text-white text-2xl mt-3 max-w-4xl">{title2}</p>
        <Image className="mt-16 mb-10" src={satellite} alt="satellite" />
      </div>
      <section className="missions flex ml-10 mr-10 pl-10 pr-10 justify-center">
        <Clients props={clients} />
      </section>
      <Footer />
    </div>
  );
}

// export const getStaticProps = async () => {
//   return {
//     props: {
//       meta: defaultMetaProps,
//     },
//   };
// };
