export default function Clients({ props }) {
  // console.log('props',props[0].properties.Description.rich_text[0].plain_text)
  return (
    <>
      {props?.map((client) => (
        <div className="max-w-sm ml-10 mr-10 rounded-6xl overflow-hidden shadow-lg backdrop-opacity-10 backdrop-invert bg-slate/60" key={`Client-${client.id}`}>
          <div className="ml-6 mr-6 mt-6">
            {/* <Image className="w-full rounded-6xl" src={client.properties.image.files[0].file.url} alt="Sunset in the mountains" layout='fill'/> */}
            <img className="w-full rounded-6xl" src={client.properties.image.files[0].file.url} alt="Sunset in the mountains" />
          </div>
          <div className="px-6 py-4 text-center">
            <div className="font-bold text-xl mb-2 text-elr-violet">{client.properties.Name.title[0].plain_text}</div>
            <p className="text-white">{client.properties.Description.rich_text[0].plain_text}</p>
          </div>
          <div className="px-6 pt-4 pb-8 flex justify-center">
            <button className=" bg-elr-intensepink rounded-full p-3 pr-4 pl-4 inline-flex items-center w-auto text-white">
              <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-6 h-6 mr-2">
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M2.036 12.322a1.012 1.012 0 010-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178z"
                />
                <path strokeLinecap="round" strokeLinejoin="round" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
              </svg>

              <span className="">Ver misión</span>
            </button>
          </div>
        </div>

        // <div className="max-w-sm ml-10 mr-10 rounded-6xl overflow-hidden shadow-lg backdrop-opacity-10 backdrop-invert bg-slate/60">
        //   <div className="ml-6 mr-6 mt-6">
        //     <Image className="w-full rounded-6xl" src={cardTop} alt="Sunset in the mountains" />
        //   </div>
        //   <div className="px-6 py-4 text-center">
        //     <div className="font-bold text-xl mb-2 text-elr-violet">The Coldest Sunset</div>
        //     <p className="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus quia, nulla! Maiores et perferendis eaque, exercitationem praesentium nihil.</p>
        //   </div>
        //   <div className="px-6 pt-4 pb-8 flex justify-center">
        //     <button className=" bg-elr-intensepink rounded-full p-3 pr-4 pl-4 inline-flex items-center w-auto text-white">
        //       <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-6 h-6 mr-2">
        //         <path
        //           strokeLinecap="round"
        //           strokeLinejoin="round"
        //           d="M2.036 12.322a1.012 1.012 0 010-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178z"
        //         />
        //         <path strokeLinecap="round" strokeLinejoin="round" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
        //       </svg>

        //       <span className="">Ver misión</span>
        //     </button>
        //   </div>
        // </div>

        // <div className="max-w-sm ml-10 mr-10 rounded-6xl overflow-hidden shadow-lg backdrop-opacity-10 backdrop-invert bg-slate/60">
        //   <div className="ml-6 mr-6 mt-6">
        //     <Image className="w-full rounded-6xl" src={cardTop} alt="Sunset in the mountains" />
        //   </div>
        //   <div className="px-6 py-4 text-center">
        //     <div className="font-bold text-xl mb-2 text-elr-violet">The Coldest Sunset</div>
        //     <p className="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus quia, nulla! Maiores et perferendis eaque, exercitationem praesentium nihil.</p>
        //   </div>
        //   <div className="px-6 pt-4 pb-8 flex justify-center">
        //     <button className=" bg-elr-intensepink rounded-full p-3 pr-4 pl-4 inline-flex items-center w-auto text-white">
        //       <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-6 h-6 mr-2">
        //         <path
        //           strokeLinecap="round"
        //           strokeLinejoin="round"
        //           d="M2.036 12.322a1.012 1.012 0 010-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178z"
        //         />
        //         <path strokeLinecap="round" strokeLinejoin="round" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
        //       </svg>

        //       <span className="">Ver misión</span>
        //     </button>
        //   </div>
        // </div>
      ))}
    </>
  );
}
